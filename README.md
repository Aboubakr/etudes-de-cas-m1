Ce dépôt contient les supports de cours et les codes associés à mes deux cours d'« étude de cas » : 
- celui sur les enregistrements extracellulaires et le tri des potentiel d'action ; 
- celui sur les mesure de concentration calciques au moyen de sondes fluorescentes.
